module CleanLong where

type Formula = Int -> Int

fuelFormula1 :: Formula
fuelFormula1 mass = div mass 3 - 2

fuelFormula2 :: Formula
fuelFormula2 mass
  | 0 >= fuel = 0
  | otherwise = fuel + fuelFormula2 fuel
  where fuel = fuelFormula1 mass

readInput :: IO [Int]
readInput = fmap read <$> readLines "data"
  where readLines = fmap lines . readFile

fuelRequiredBy :: Formula -> IO Int
fuelRequiredBy parseType = sum . map parseType <$> readInput

displayRequiredFuelBy :: Formula -> IO ()
displayRequiredFuelBy formula = do
  total <- fuelRequiredBy formula
  print total

main :: IO ()
main = do
  displayRequiredFuelBy fuelFormula1
  displayRequiredFuelBy fuelFormula2
