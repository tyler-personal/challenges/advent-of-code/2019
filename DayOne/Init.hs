module Init where

readInput :: IO [Int]
readInput = fmap read <$> readLines "data"

readLines :: FilePath -> IO [String]
readLines = fmap lines . readFile

main :: IO ()
main = do
  partOne
  partTwo

parseOne x = div x 3 - 2

partX f = readInput >>= print . sum . map f

partOne = partX parseOne

parseTwo x
  | parsed <= 0 = 0
  | otherwise = parsed + parseTwo parsed
  where parsed = parseOne x

partTwo = partX parseTwo
