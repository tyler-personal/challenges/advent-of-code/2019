print(list(map(lambda a: sum(a(int(line)) for line in open('data', 'r')), [f := lambda m: m // 3 - 2, g := lambda m: 0 if (0 >= (n := f(m))) else n + g(n)])))
