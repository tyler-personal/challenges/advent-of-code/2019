{-# LANGUAGE ExtendedDefaultRules, TypeApplications #-}
module Short where

main=let f x=div x 3-2;g x = let a = f x in if 0 >= a then 0 else a + g a in mapM (\f->sum.map f<$>((<$>)read.lines<$>readFile "data")) [f,g] >>= print

maiz=mapM(\f->sum.map f<$>((<$>)read.lines<$>readFile "DayOne_Input.txt")) [f,\a->let b=f a in if 0>b then 0 else b+f b]>>=print where f x=div x 3-2

res = mapM (\f -> sum . map f <$> (fmap read . lines <$> readFile ""))
  [f, \a -> let b = f a in if 0 > b then 0 else b + f b] >>= print
  where f x = div x 3 - 2

