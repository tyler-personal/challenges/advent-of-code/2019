{-# LANGUAGE ExtendedDefaultRules, TypeApplications #-}
module Clean where

type Formula = Int -> Int

formula1 :: Formula
formula1 mass = mass `div` 3 - 2

formula2 :: Formula
formula2 mass
  | 0 >= fuel = 0
  | otherwise = fuel + formula2 fuel
  where fuel = formula1 mass

f = sum ./. map

(./.) = (.) . (.)

totalFuel :: Formula -> IO Int
totalFuel formula = do
  input <- fmap read <$> fmap lines (readFile "data")
  return . sum . map formula $ input

main :: IO ()
main = do
  totalFuel formula1 >>= print
  totalFuel formula2 >>= print
