{-# LANGUAGE TupleSections, PatternSynonyms #-}
import Data.Sequence (update, fromList, Seq(..), index)
import Data.Foldable (find)
import Data.Maybe (fromJust)

process codes (opcode :<| nounAddress :<| verbAddress :<| outputAddress :<| rest) =
  case opcode of
    1  -> codes' (+)
    2  -> codes' (*)
    99 -> codes
  where
    [noun, verb] = map (index codes) [nounAddress, verbAddress]
    codes' f = process (update outputAddress (f noun verb) codes) rest

findOutput codes noun verb = index (process codes' codes') 0
  where codes' = update 1 noun (update 2 verb codes)

findInput codes output = noun * 100 + verb
  where
    options = [0..99] >>= \x -> map (x,) [0..99]
    (noun, verb) = fromJust $ find (\(x,y) -> output == findOutput codes x y) options

main = do
  print $ findOutput intCodes 12 2
  print $ findInput intCodes 19690720

intCodes = fromList [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,5,19,23,1,23,5,27,1,27,13,31,1,31,5,35,1,9,35,39,2,13,39,43,1,43,10,47,1,47,13,51,2,10,51,55,1,55,5,59,1,59,5,63,1,63,13,67,1,13,67,71,1,71,10,75,1,6,75,79,1,6,79,83,2,10,83,87,1,87,5,91,1,5,91,95,2,95,10,99,1,9,99,103,1,103,13,107,2,10,107,111,2,13,111,115,1,6,115,119,1,119,10,123,2,9,123,127,2,127,9,131,1,131,10,135,1,135,2,139,1,10,139,0,99,2,0,14,0]

