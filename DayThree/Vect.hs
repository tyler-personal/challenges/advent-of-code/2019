{-# LANGUAGE TupleSections, MonadComprehensions #-}
module Vect where
import Prelude hiding (Left, Right, map, scanl, reverse, concat, drop, last, minimum)
import Data.Vector
import Data.List.Split (splitOn)
import System.TimeIt

data Direction = Up | Down | Left | Right deriving (Show)
type Point = (Int, Int)
type Path = Vector Point
type Move = (Direction, Int)

moves :: Point -> Move -> Path
moves (x,y) (direction, a) = case direction of
  Up    -> map (x,) (fromList [y..y+a])
  Down  -> map (x,) (reverse (fromList [y-a..y]))
  Left  -> map (,y) (reverse (fromList [x-a..x]))
  Right -> map (,y) (fromList [x..x+a])

createPoints :: Vector Move -> Path
createPoints = drop 1 . uniq . concat . scanl (\p m -> moves (last p) m) (fromList [(0,0)])

shortestManhattanDistance :: Path -> Path -> Int
shortestManhattanDistance p1 p2 = minimum $ map (\(x,y) -> abs x + abs y) (intersect p1 p2)

parseLine :: String -> [Move]
parseLine line = map f $ splitOn "," line
  where
    f (d : n) = case d of
      'U' -> (Up, n')
      'D' -> (Down, n')
      'L' -> (Left, n')
      'R' -> (Right, n')
      where n' = read n

readLines :: IO ([Move], [Move])
readLines = do
  [l1, l2] <- lines <$> readFile "data"
  return (parseLine l1, parseLine l2)

main = timeIt execute
execute = do
  (l1, l2) <- readLines
  print $ shortestManhattanDistance (createPoints l1) (createPoints l2)

