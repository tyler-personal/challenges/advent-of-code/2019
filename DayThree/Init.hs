{-# LANGUAGE TupleSections #-}
import Prelude hiding (Left, Right)
import Data.List.Split (splitOn)
import Data.List (intersect, nub, find)
import Data.Maybe (fromJust)
import Debug.Trace
import System.TimeIt

data Direction = Up | Down | Left | Right deriving (Show)
type Point = (Int, Int)
type Path = [Point]
type Move = (Direction, Int)

moves :: Point -> Move -> Path
moves (x,y) (direction, a) = case direction of
  Up    -> map (x,) [y..y+a]
  Down  -> map (x,) (reverse [y-a..y])
  Left  -> map (,y) (reverse [x-a..x])
  Right -> map (,y) [x..x+a]

createPoints :: [Move] -> Path
createPoints = drop 1 . nub . concat . scanl (\p m -> moves (last p) m) [(0,0)]

shortestManhattanDistance :: Path -> Path -> Int
shortestManhattanDistance p1 p2 = minimum $ map (\(x,y) -> abs x + abs y) (intersect p1 p2)

shortestSteps :: Path -> Path -> Int
shortestSteps p1 p2 = minimum $ map (\point -> getSteps point p1 + getSteps point p2) (intersect p1 p2)
-- shortestSteps p1 p2 = minimum
  where getSteps point = fst . fromJust . find ((==) point . snd) . zip [1..]

parseLine :: String -> [Move]
parseLine line = map f $ splitOn "," line
  where
    f (d : n) = case d of
      'U' -> (Up, n')
      'D' -> (Down, n')
      'L' -> (Left, n')
      'R' -> (Right, n')
      where n' = read n

readLines :: IO ([Move], [Move])
readLines = do
  [l1, l2] <- lines <$> readFile "realdata"
  return (parseLine l1, parseLine l2)

main = do
  (l1, l2) <- readLines
--  print $ shortestManhattanDistance (createPoints l1) (createPoints l2) -- part 1
  print $ shortestSteps (createPoints l1) (createPoints l2) -- part 2

t' x = trace (show x) x
