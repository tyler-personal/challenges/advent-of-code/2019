{-# LANGUAGE QuasiQuotes #-}
import Data.Digits
import Control.Applicative
import Data.String.Interpolate
import Debug.Trace

isValid n = containsDouble && neverDecreases
  where
    pairedDigits = (zip <*> tail) (digits 10 n)
    containsDouble = any (uncurry (==)) pairedDigits
    neverDecreases = all (uncurry (<=)) pairedDigits


isValid' n = isValid n && containsExactlyDouble
  where
    containsExactlyDouble = f (-1) (digits 10 n) 0

    f _ [] 1 = True
    f _ [] _ = False
    f lastN (x:xs) count
      | lastN /= x && count == 1 = True
      | lastN == x = g (count + 1)
      | otherwise = g 0
      where g = f x xs

partOne = length . filter isValid
partTwo = length . filter isValid'

--g = length . filter (liftA2 (&&) (any (uncurry (==))) (all (uncurry (<=))) . (zip <*> tail) . digits 10)
main = undefined
